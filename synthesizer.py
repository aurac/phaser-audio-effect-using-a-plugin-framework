import numpy as np
import pyaudio
import threading
import time
import tkinter as tk
from scipy import signal

class Synthesizer:
    
    """
    A simple synthesizer class that allows generating and playing sound waves.
    """
    def __init__(self, sample_rate=44100, duration=5):
        
        """
        Initializes the synthesizer with default parameters.

        Parameters:
            sample_rate (int): The sample rate of the audio in Hz (default is 44100).
            duration (float): The duration of the generated sound in seconds (default is 5).
        """
        self.sample_rate = sample_rate
        self.duration = duration
        self.amplitude = 0.5
        self.frequency = 440
        self.modulation = 0.0
        self.modulation_rate = 5
        self.root = tk.Tk()
        self.root.title("Synthesizer")

        # Labels and Entries for Frequency and Amplitude
        self.label_frequency = tk.Label(self.root, text="Frequency (Hz):")
        self.label_frequency.grid(row=0, column=0)
        self.entry_frequency = tk.Entry(self.root)
        self.entry_frequency.insert(0, str(self.frequency))
        self.entry_frequency.grid(row=0, column=1)

        self.label_amplitude = tk.Label(self.root, text="Amplitude:")
        self.label_amplitude.grid(row=1, column=0)
        self.entry_amplitude = tk.Entry(self.root)
        self.entry_amplitude.insert(0, str(self.amplitude))
        self.entry_amplitude.grid(row=1, column=1)

        # Dropdown for Waveform Selection
        self.label_waveform = tk.Label(self.root, text="Waveform:")
        self.label_waveform.grid(row=2, column=0)
        self.waveform_var = tk.StringVar(self.root)
        self.waveform_var.set("Sine")
        self.waveform_dropdown = tk.OptionMenu(self.root, self.waveform_var, "Sine", "Square", "Sawtooth")
        self.waveform_dropdown.grid(row=2, column=1)

        # Envelope Controls
        self.label_attack = tk.Label(self.root, text="Attack:")
        self.label_attack.grid(row=3, column=0)
        self.attack_scale = tk.Scale(self.root, from_=0, to=100, orient=tk.HORIZONTAL)
        self.attack_scale.grid(row=3, column=1)

        self.label_decay = tk.Label(self.root, text="Decay:")
        self.label_decay.grid(row=4, column=0)
        self.decay_scale = tk.Scale(self.root, from_=0, to=100, orient=tk.HORIZONTAL)
        self.decay_scale.grid(row=4, column=1)

        self.label_sustain = tk.Label(self.root, text="Sustain:")
        self.label_sustain.grid(row=5, column=0)
        self.sustain_scale = tk.Scale(self.root, from_=0, to=100, orient=tk.HORIZONTAL)
        self.sustain_scale.grid(row=5, column=1)

        self.label_release = tk.Label(self.root, text="Release:")
        self.label_release.grid(row=6, column=0)
        self.release_scale = tk.Scale(self.root, from_=0, to=100, orient=tk.HORIZONTAL)
        self.release_scale.grid(row=6, column=1)

        # Play Button
        self.button_play = tk.Button(self.root, text="Play", command=self.play)
        self.button_play.grid(row=7, columnspan=2)

        self.root.mainloop()

    def generate_wave(self, frequency, duration):
        """
        Generates a sound wave with the specified frequency and duration.

        Parameters:
            frequency (float): The frequency of the wave in Hz.
            duration (float): The duration of the wave in seconds.

        Returns:
            numpy.ndarray: An array containing the generated sound wave.
        """
        time = np.linspace(0, duration, int(duration * self.sample_rate), endpoint=False)
        if self.waveform_var.get() == "Sine":
            wave = self.amplitude * np.sin(2 * np.pi * frequency * time + self.modulation)
        elif self.waveform_var.get() == "Square":
            wave = self.amplitude * signal.square(2 * np.pi * frequency * time + self.modulation)
        elif self.waveform_var.get() == "Sawtooth":
            wave = self.amplitude * signal.sawtooth(2 * np.pi * frequency * time + self.modulation)
        return wave

    def generate_envelope(self):
        
        """
        Generates an envelope for the sound wave based on the attack, decay,
        sustain, and release parameters.

        Returns:
            numpy.ndarray: An array containing the generated envelope.
        """
        
        attack = np.linspace(0, self.amplitude, int(self.attack_scale.get() / 100 * self.sample_rate))
        decay = np.linspace(self.amplitude, self.sustain_scale.get() / 100 * self.amplitude, int(self.decay_scale.get() / 100 * self.sample_rate))
        sustain = np.full(int(self.duration * self.sustain_scale.get() / 100 * self.sample_rate), self.sustain_scale.get() / 100 * self.amplitude)
        release = np.linspace(self.sustain_scale.get() / 100 * self.amplitude, 0, int(self.release_scale.get() / 100 * self.sample_rate))
        envelope = np.concatenate((attack, decay, sustain, release))
        
        # Adjust envelope length to match wave length
        if len(envelope) > len(self.generate_wave(self.frequency, self.duration)):
            envelope = envelope[:len(self.generate_wave(self.frequency, self.duration))]
        else:
            envelope = np.pad(envelope, (0, len(self.generate_wave(self.frequency, self.duration)) - len(envelope)), 'constant')
        
        return envelope

    def update_modulation(self):
        
        """
        Continuously updates the modulation parameter to create dynamic changes
        in the sound wave.
        """
        
        while True:
            self.modulation = 0.1 * np.sin(2 * np.pi * self.modulation_rate * time.time())
            time.sleep(0.01)

    def play_sound(self, sound):
        
        """
        Plays the given sound wave.

        Parameters:
            sound (numpy.ndarray): An array containing the sound wave to be played.
        """
        
        audio = pyaudio.PyAudio()
        stream = audio.open(format=pyaudio.paFloat32,
                            channels=1,
                            rate=self.sample_rate,
                            output=True)
        stream.write(sound.astype(np.float32).tobytes())
        stream.close()
        audio.terminate()

    def play(self):
        
        """
        Main function to generate, modulate, and play the sound wave based on user input.
        """
        self.frequency = float(self.entry_frequency.get())
        self.amplitude = float(self.entry_amplitude.get())

        wave = self.generate_wave(self.frequency, self.duration)
        envelope = self.generate_envelope()

        modulation_thread = threading.Thread(target=self.update_modulation)
        modulation_thread.start()

        sound = wave * envelope

        self.play_sound(sound)

if __name__ == "__main__":
    synth = Synthesizer()

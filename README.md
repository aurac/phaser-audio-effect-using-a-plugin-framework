# Sound Synthesizer
Author: Aura Castellanos Calderon

## Project Description

This project implements a sound synthesizer in Python using the Tkinter library for the graphical user interface. The synthesizer allows generating and playing sounds of different waveforms (sine, square, sawtooth) with the ability to adjust parameters such as frequency, amplitude, and envelope.

## Project Files

- `synthesizer.py`: Main program implementing the sound synthesizer.
- `README.md`: This file containing the project description.

## Features

- Waveform Selection: User can choose between three different waveforms (sine, square, sawtooth).
- Frequency and Amplitude Control: User can adjust the frequency and amplitude of the generated sound.
- Envelope Control: User can configure attack, decay, sustain, and release parameters to modulate the shape of the generated sound.
- Real-time Sound Playback: The synthesizer plays the generated sound in real-time through the device's audio system.
- Intuitive Graphical Interface: The graphical interface provides intuitive visual controls for adjusting synthesizer parameters.

## Usage

To run the program, simply execute the `synthesizer.py` file. A graphical interface window will open where you can adjust the synthesizer parameters and play the generated sound.

To stop sound playback, you can close the graphical interface window.

## Additional Notes

- This sound synthesizer uses the PyAudio library for real-time audio playback.
- The program utilizes the NumPy library for generating and manipulating audio waveforms.
- The Tkinter library is used to create the graphical interface of the synthesizer.
- This project can be extended in the future to include more features and advanced options, such as integrating audio effects, implementing new types of waveform shapes, among others.








